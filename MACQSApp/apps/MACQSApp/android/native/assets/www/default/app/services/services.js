
/* JavaScript content from app/services/services.js in folder common */
var ORDER_COLLECTION_NAME = 'orderNumbers',
			KEY_VALUE_COLLECTION_NAME = 'keyvalue';
var USER;
var USER_PW ; 
var FETO_COLLECTION_NAME = 'fetos';
var FORMULAR1_COLLECTION_NAME = 'formular1';
var onlineLo = false ;
var feto_serv ; 
angular.module('starter.services', [])
.factory(
		'LoginService',
		function($q) {
			
			return {
		        loginUser: function(name, pw) {
		            var deferred = $q.defer();
		            var promise = deferred.promise;
		 console.log("My Service :  name " , name);
		 name = 'user';
		 pw = '';
		 // verbindung zur LDAP or zur Collection

//		 WL.Device.getNetworkInfo(
//					function (networkInfo) {
//						console.log("Test WL.Device.getNetworkInfo  " , networkInfo);
//						
//						console.log("Test Device  networkInfo.isNetworkConnected   " , networkInfo.isNetworkConnected );
//						if (networkInfo.isNetworkConnected == "true") {
//							//onlineAuthentication();
//						} else {
//							//offlineAuthentication();
//						}
//					}
//				);
		            if (name == 'user' && pw == 'secret') {
		                deferred.resolve('Welcome ' + name + '!');
		            } else {
		                deferred.reject('Wrong credentials.');
		            }
		            promise.success = function(fn) {
		                promise.then(fn);
		                return promise;
		            }
		            promise.error = function(fn) {
		                promise.then(null, fn);
		                return promise;
		            }
		            return promise;
		        }
		    }

		})
	.factory(
    		'HomeService',
    		function($q) {

    			var items = [];
    			var item = {};
    			
//    			function isOnline(){
//    				var deferred = $q.defer();
//    			    WL.Client.connect({
//    			        onSuccess:function() {
//    			        	console.log("we are saved ");
//    			        	 onlineLo = true ; 
//    			        	 deferred.resolve(onlineLo);     
//    			        }, 
//    			        
//    			        onFailure:function(f) {
//    			        	onlineLo = false ; 
//    			        	deferred.resolve(onlineLo);
//    			        },
//    			        });
//    			    return deferred.promise;
//    				
//    			}
    			
    			function getAllFanr(feto) {
    				console.log(" feto ", feto);
    				var deferred = $q.defer();
    				var req = new WLResourceRequest("/adapters/FANR/getFanr",
    						WLResourceRequest.GET);

    				req.send().then(function(res) {
    					console.log("items  fanr ", res.responseJSON.array);
    					items = res.responseJSON.array;
    					deferred.resolve(items);
    				}, function(bad) {
    					console.log("bad");
    					console.dir(bad);
    					deferred.reject("um something here");
    				});

    				console.log("deferred.promise ! ", deferred.promise);
    				return deferred.promise;
    			}
    			
    			// read all Feto
    			
    			function getAllFeto() {
				var deferred = $q.defer();
				var req = new WLResourceRequest("/adapters/Feto/getFeto",
						WLResourceRequest.GET);

				req.send().then(function(res) {
					console.log(" items  feto as TEST : ", res.responseJSON.array);
					items = res.responseJSON.array;
					deferred.resolve(items);
				}, function(bad) {
					console.log("bad");
					console.dir(bad);
					deferred.reject("um something here");
				});

				console.log("deferred.promise ! ", deferred.promise);

				return deferred.promise;
			}
    			
    			function sendAuftragNr(fanr) {
    				var deferred = $q.defer();
    				var frnStr = fanr.toString().trim();
    				var req = new WLResourceRequest("/adapters/FANR/sendAuftragNr",
    						WLResourceRequest.GET);
    				
   				req.setQueryParameter("params", "["+ "'"+frnStr +"'" +"]");
    				req.send().then(function(res) {
    					console.log("sendAuftragNr  resp ", res.responseJSON.array);
    					item = res.responseJSON;
    					console.log(" items empty! ", item.fanr);
    					deferred.resolve(item);
    				}, function(bad) {
    					console.log("bad");
    					console.dir(bad);
    					deferred.reject("um something here");
    				});

    				console.log(" sendAuftragNr deferred.promise ! ", deferred.promise);

    				return deferred.promise;
    			}
    			
    			return {
    				getAllFanr:getAllFanr,
    				getAllFeto:getAllFeto,
    				sendAuftragNr: sendAuftragNr
//    				isOnline: isOnline
    			};

    		})

// test jSONStore....
.factory(
		'JsonStoreService',
		function($q) {
			var items = [];
			var item = {};
			function getAllFanr(feto) {
				var deferred = $q.defer();
				console.log("feto", feto);
				if(feto!=""){
					feto_serv = feto;
                    WL.JSONStore.get(ORDER_COLLECTION_NAME).findAll().then(function (res) {
                    	console.log("my JSONStore  res ", res);
//    					items = JSON.stringify(res);
//    					console.log(" JSON.stringify(res)! ", items);
//    					console.log("res with data! ", JSON.parse(JSON.stringify(res)));
    					deferred.resolve(res);
    				}, function(bad) {
    					console.log("bad");
    					console.dir(bad);
    					deferred.reject("um something here");
    				});
				//  });
				}
                    console.log("deferred.promise ! ", deferred.promise);
				return deferred.promise;
			}
			
			function getAllFeto() {
				var deferred = $q.defer();
				WL.JSONStore.init(collections).then(function(){
                WL.JSONStore.get(FETO_COLLECTION_NAME).findAll().then(function (res) {
              	console.log(" feto services data : ", res);
					deferred.resolve(res);
				}, function(bad) {
					console.log("bad");
					console.dir(bad);
					deferred.reject("um something here");
				});
			  });
                console.log("deferred.promise ! ", deferred.promise);
			return deferred.promise;
			}
			
			
			
			function sendAuftragNr(fanr) {
				var deferred = $q.defer();
				var frnStr = fanr.toString().trim();
				var fanrStr = "+frnStr+";
				console.log("formularhead with fanr :  ", frnStr ," and feto : " , feto_serv );
				var query = {feto: feto_serv};
				WL.JSONStore.init(collections).then(function(){
					
//					var options = {
//					    exact: false,
//					    limit: 1 //returns a maximum of 10 documents
//					}; 

	                WL.JSONStore.get(FORMULAR1_COLLECTION_NAME).find(query).then(function (res) {
	              	console.log("sendAuftragNr :  formularhead services data : ", res);
						deferred.resolve(res);
					}, function(bad) {
						console.log("bad");
						console.dir(bad);
						deferred.reject("um something here");
					});
				  });
	               

//				var req = new WLResourceRequest("/adapters/FANR/sendAuftragNr",
//						WLResourceRequest.GET);
//				
//				req.setQueryParameter("params", "["+ "'"+frnStr +"'" +"]");
//				//req.setQueryParameter("params", "['K203YI06']");
//				
//
//				req.send().then(function(res) {
//					console.log("sendAuftragNr  resp ", res.responseJSON.array);
//					item = res.responseJSON;
//					console.log(" items empty! ", item.fanr);
//					deferred.resolve(item);
//				}, function(bad) {
//					console.log("bad");
//					console.dir(bad);
//					deferred.reject("um something here");
//				});


				return deferred.promise;
			}
			
			return {
				getAllFanr:getAllFanr,
				getAllFeto:getAllFeto,
				sendAuftragNr: sendAuftragNr
			};

		});	
