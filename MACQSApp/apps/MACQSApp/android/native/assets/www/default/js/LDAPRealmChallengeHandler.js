
/* JavaScript content from js/LDAPRealmChallengeHandler.js in folder common */
       /*
        *

        */
var LDAPRealmChallengeHandler = WL.Client.createChallengeHandler("LDAPRealm");
var myTestVari ;

console.log("TEST Offline Authentification with LDAP ....."); 

LDAPRealmChallengeHandler.isCustomResponse = function(response) {
	myTestVari = response.responseText ;
	
    if (!response || !response.responseText) {
        return false;
    }
    
    var idx = response.responseText.indexOf("j_security_check");
    
    if (idx >= 0){ 
    	return true;
    }
    return false;

};

console.log("TEST Offline Authentification.2.myTestVari.", myTestVari);

LDAPRealmChallengeHandler.handleChallenge = function(response){	
	console.log("Was ist in LDAPRealmChallengeHandler : ");

	// Authentication required, display the login form and the online login button.
	//if (authRequired == true){
		if (!($("#authDiv").is(":visible"))) { 
			$("#unsecuredDiv").hide();
			$("#authDiv").show();
			$("#offlineLoginButton").hide();
			$("#onlineLoginButton").show();
		}
		
		$("#username").val('');
		$("#password").val('');
		$("#authInfo").empty();

//		if (response.responseText.errorMessage) {
//	    	$("#authInfo").html(response.responseText.errorMessage);
//		}
		
	//} else if (authRequired == false){
		//LDAPRealmChallengeHandler.submitSuccess();
	//}
};

$("#onlineLoginButton").bind('click', function () {
    var reqURL = '/j_security_check';
    var options = {};
    options.parameters = {
    		j_username : $('#username').val(),
    		j_password : $('#password').val()
    };
    options.headers = {};
    
    LDAPRealmChallengeHandler.submitLoginForm(reqURL, options, LDAPRealmChallengeHandler.submitLoginFormCallback);
});

$('#cancelButton').bind('click', function () {
	$("#authDiv").hide();
    $("#unsecuredDiv").show();
	LDAPRealmChallengeHandler.submitFailure();
    console.log("username : " , $('#username').val());
    
});

//LDAPRealmChallengeHandler.submitLoginFormCallback = function(response) {
//    var isLoginFormResponse = LDAPRealmChallengeHandler.isCustomResponse(response);
//    if (isLoginFormResponse){
//    	LDAPRealmChallengeHandler.handleChallenge(response);
//    } else {
//    	$('#unsecuredDiv').show();
//    	$('#AuthDiv').hide();
//    	LDAPRealmChallengeHandler.submitSuccess();
//    }
//};