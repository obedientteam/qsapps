
/* JavaScript content from app/services_old/services.js in folder common */
var ORDER_COLLECTION_NAME = 'orders',
			KEY_VALUE_COLLECTION_NAME = 'keyvalue';
var USER;
var USER_PW ; 

angular.module('starter.services', [])
.factory(
		'LoginService',
		function($q) {
			
			return {
		        loginUser: function(name, pw) {
		            var deferred = $q.defer();
		            var promise = deferred.promise;
		 console.log("My Service :  name " , name);
		 name = 'user';
		 pw = 'secret';
		 // verbindung zur LDAP or zur Collection
//		 if (){
//			 
//		 }else{
//			 
//		 }
		            if (name == 'user' && pw == 'secret') {
		                deferred.resolve('Welcome ' + name + '!');
		            } else {
		                deferred.reject('Wrong credentials.');
		            }
		            promise.success = function(fn) {
		                promise.then(fn);
		                return promise;
		            }
		            promise.error = function(fn) {
		                promise.then(null, fn);
		                return promise;
		            }
		            return promise;
		        }
		    }

		})
	.factory(
    		'HomeService',
    		function($q) {

    			var items = [];
    			var item = {};
    			
    			function getAllFanr() {
    				var deferred = $q.defer();
    				var req = new WLResourceRequest("/adapters/FANR/getFanr",
    						WLResourceRequest.GET);

    				req.send().then(function(res) {
    					console.log("items  fanr ", res.responseJSON.array);
    					items = res.responseJSON.array;
    					deferred.resolve(items);
    				}, function(bad) {
    					console.log("bad");
    					console.dir(bad);
    					deferred.reject("um something here");
    				});

    				console.log("deferred.promise ! ", deferred.promise);
    				return deferred.promise;
    			}
    			
    			// read all Feto
    			
    			function getAllFeto() {
				var deferred = $q.defer();
				var req = new WLResourceRequest("/adapters/Feto/getFeto",
						WLResourceRequest.GET);

				req.send().then(function(res) {
					console.log(" items  feto ", res.responseJSON.array);
					items = res.responseJSON.array;
					deferred.resolve(items);
				}, function(bad) {
					console.log("bad");
					console.dir(bad);
					deferred.reject("um something here");
				});

				console.log("deferred.promise ! ", deferred.promise);

				return deferred.promise;
			}
    			
    			function sendAuftragNr(fanr) {
    				var deferred = $q.defer();
    				var frnStr = fanr.toString().trim();
    				var req = new WLResourceRequest("/adapters/FANR/sendAuftragNr",
    						WLResourceRequest.GET);
    				
   				req.setQueryParameter("params", "["+ "'"+frnStr +"'" +"]");
    				req.send().then(function(res) {
    					console.log("sendAuftragNr  resp ", res.responseJSON.array);
    					item = res.responseJSON;
    					console.log(" items empty! ", item.fanr);
    					deferred.resolve(item);
    				}, function(bad) {
    					console.log("bad");
    					console.dir(bad);
    					deferred.reject("um something here");
    				});

    				console.log(" sendAuftragNr deferred.promise ! ", deferred.promise);

    				return deferred.promise;
    			}
    			
    			return {
    				getAllFanr:getAllFanr,
    				getAllFeto:getAllFeto,
    				sendAuftragNr: sendAuftragNr
    			};

    		})

// test jSONStore....
.factory(
		'JsonStoreService',
		function($q) {

			var items = [];
			var item = {};
			
			function getAllFanr() {
				var deferred = $q.defer();

//				var req = new WLResourceRequest("/adapters/FANR/getFanr",
//						WLResourceRequest.GET);
//
//				req.send().then(function(res) {
//					console.log("my items  res ", res.responseJSON.array);
//					items = res.responseJSON.array;
//					console.log("items empty! ", items[0].name);
//					deferred.resolve(items);
//				}, function(bad) {
//					console.log("bad");
//					console.dir(bad);
//					deferred.reject("um something here");
//				});

				//initJSONStore().then(function(){
				
                    WL.JSONStore.get(ORDER_COLLECTION_NAME).findAll().then(function (res) {
                    	console.log("my JSONStore  res ", res);
    					items = JSON.stringify(res);
    					console.log(" JSON.stringify(res)! ", items);
    					console.log("items with data! ", items);
    					deferred.resolve(items);
    				}, function(bad) {
    					console.log("bad");
    					console.dir(bad);
    					deferred.reject("um something here");
    				});
				//  });
				console.log("deferred.promise ! ", deferred.promise);
				return deferred.promise;
			}
			
			
			
			function sendAuftragNr(fanr) {
				var deferred = $q.defer();
				var frnStr = fanr.toString().trim();
				var fanrStr = "+frnStr+";
				console.log("sendAuftragNr with :  ", frnStr);

				var req = new WLResourceRequest("/adapters/FANR/sendAuftragNr",
						WLResourceRequest.GET);
				
				req.setQueryParameter("params", "["+ "'"+frnStr +"'" +"]");
				//req.setQueryParameter("params", "['K203YI06']");
				

				req.send().then(function(res) {
					console.log("sendAuftragNr  resp ", res.responseJSON.array);
					item = res.responseJSON;
					console.log(" items empty! ", item.fanr);
					deferred.resolve(item);
				}, function(bad) {
					console.log("bad");
					console.dir(bad);
					deferred.reject("um something here");
				});

				console.log(" sendAuftragNr deferred.promise ! ", deferred.promise);

				return deferred.promise;
			}
			
			return {
				getAllFanr:getAllFanr,
				sendAuftragNr: sendAuftragNr
			};

		});	
