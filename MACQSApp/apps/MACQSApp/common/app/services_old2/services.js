var ORDER_COLLECTION_NAME = 'orderNumbers',
			KEY_VALUE_COLLECTION_NAME = 'keyvalue';

var USER_COLLECTION_NAME = 'users';
var USER_PW ; 
var FETO_COLLECTION_NAME = 'fetos';

angular.module('starter.services', [])
.factory(
		'LoginService',
		function($q) {
			
			return {
		        loginUser: function(name, pw) {
		            var deferred = $q.defer();
		            var promise = deferred.promise;
		 console.log("My Service :  name " , name);
		 name = 'user';
		 pw = 'secret';
		 // verbindung zur LDAP or zur Collection
//		 if (){
//			 
//		 }else{
//			 
//		 }
		            if (name == 'user' && pw == 'secret') {
		                deferred.resolve('Welcome ' + name + '!');
		            } else {
		                deferred.reject('Wrong credentials.');
		            }
		            promise.success = function(fn) {
		                promise.then(fn);
		                return promise;
		            }
		            promise.error = function(fn) {
		                promise.then(null, fn);
		                return promise;
		            }
		            return promise;
		        }
		    }

		})
	.factory(
    		'HomeService',
    		function($q) {

    			var items = [];
    			var item = {};
    			
    			function getAllFanr() {
    				var deferred = $q.defer();
    				var req = new WLResourceRequest("/adapters/fanrs/getFanrs",
    						WLResourceRequest.GET);

    				req.send().then(function(res) {
    					console.log("items  fanr ", res.responseJSON.array);
    					items = res.responseJSON.array;
    					deferred.resolve(items);
    				}, function(bad) {
    					console.log("bad");
    					console.dir(bad);
    					deferred.reject("um something here");
    				});

    				console.log("deferred.promise ! ", deferred.promise);
    				return deferred.promise;
    			}
    			
    			// read all Feto
    			
    			function getAllFeto() {
				var deferred = $q.defer();
				var req = new WLResourceRequest("/adapters/fetos/getFetos",
						WLResourceRequest.GET);

				req.send().then(function(res) {
					console.log(" items  feto as TEST : ", res.responseJSON.array);
					items = res.responseJSON.array;
					deferred.resolve(items);
				}, function(bad) {
					console.log("bad");
					console.dir(bad);
					deferred.reject("um something here");
				});

				console.log("deferred.promise ! ", deferred.promise);

				return deferred.promise;
			}
    			
    			function sendAuftragNr(fanr) {
    				var deferred = $q.defer();
    				var frnStr = fanr.toString().trim();
    				var req = new WLResourceRequest("/adapters/fanrs/sendAuftragNr",
    						WLResourceRequest.GET);
    				
   				req.setQueryParameter("params", "["+ "'"+frnStr +"'" +"]");
    				req.send().then(function(res) {
    					console.log("sendAuftragNr  resp ", res.responseJSON.array);
    					item = res.responseJSON;
    					console.log(" items empty! ", item.fanr);
    					deferred.resolve(item);
    				}, function(bad) {
    					console.log("bad");
    					console.dir(bad);
    					deferred.reject("um something here");
    				});

    				console.log(" sendAuftragNr deferred.promise ! ", deferred.promise);

    				return deferred.promise;
    			}
    			
    			return {
    				getAllFanr:getAllFanr,
    				getAllFeto:getAllFeto,
    				sendAuftragNr: sendAuftragNr
    			};

    		})

// test jSONStore....
.factory(
		'JsonStoreService',
		function($q) {

			var items = [];
			var item = {};
			
			function getAllFanr(fanr) {
				var deferred = $q.defer();
				console.log(" fanr ", fanr);
				if(fanr!=""){
                    WL.JSONStore.get(ORDER_COLLECTION_NAME).findAll().then(function (res) {
                    console.log("my JSONStore  res ", res);
//    					items = JSON.stringify(res);
//    					console.log(" JSON.stringify(res)! ", items);
//    					console.log("res with data! ", JSON.parse(JSON.stringify(res)));
    					deferred.resolve(res);
    				}, function(bad) {
    					console.log("bad");
    					console.dir(bad);
    					deferred.reject("um something here");
    				});
				//  });
				}
                    console.log("deferred.promise ! ", deferred.promise);
				return deferred.promise;
			}
			
			function getAllFeto() {
				var deferred = $q.defer();
                WL.JSONStore.get(FETO_COLLECTION_NAME).findAll().then(function (res) {
//                	//console.log("my JSONStore  res ", res);
					deferred.resolve(res);
				}, function(bad) {
					console.log("bad");
					console.dir(bad);
					deferred.reject("um something here");
				});
			//  });
                console.log("deferred.promise ! ", deferred.promise);
			return deferred.promise;
			}
			
			
			
			function sendAuftragNr(fanr) {
				var deferred = $q.defer();
				var frnStr = fanr.toString().trim();
				var fanrStr = "+frnStr+";
				console.log("sendAuftragNr with :  ", frnStr);

				var req = new WLResourceRequest("/adapters/FANR/sendAuftragNr",
						WLResourceRequest.GET);
				
				req.setQueryParameter("params", "["+ "'"+frnStr +"'" +"]");
				//req.setQueryParameter("params", "['K203YI06']");
				

				req.send().then(function(res) {
					console.log("sendAuftragNr  resp ", res.responseJSON.array);
					item = res.responseJSON;
					console.log(" items empty! ", item.fanr);
					deferred.resolve(item);
				}, function(bad) {
					console.log("bad");
					console.dir(bad);
					deferred.reject("um something here");
				});

				console.log("sendAuftragNr deferred.promise ! ", deferred.promise);

				return deferred.promise;
			}
			
			return {
				getAllFanr:getAllFanr,
				getAllFeto:getAllFeto,
				sendAuftragNr: sendAuftragNr
			};

		});	
