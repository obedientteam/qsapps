
	 angular.module('starter', ['ionic','starter.controllers','starter.services'])

	.config(function($stateProvider, $urlRouterProvider) {
 
	 $stateProvider.state('login', {
	      url: "/login",
	      templateUrl: 'app/login/login.html',
	      controller: 'LoginController'
	  })
  
	  
//	  .state('login', {
//	    url: "/login",
//	    templateUrl: 'app/login/login.html',
//	    // If you wanted some AngularJS controller behaviour...
//	     controller: 'LoginController'
//	  })
     .state('home', {
	    url: "/home",
	    templateUrl: 'app/home/home.html',
	    // If you wanted some AngularJS controller behaviour...
	     controller: 'HomeController'
	  })
	  
	  .state('formularP1_new', {
	    url: "/formularP1_new",
	    templateUrl: 'app/formulars/formularP1_new.html',
	  //  templateUrl: 'views/formularP1.html',
	     controller: 'HomeController'
	  })
	  .state('formularP2', {
	    url: "/formularP2",
	    templateUrl: 'app/formulars/formularP2.html',
	     controller: 'HomeController'
	  })
	  .state('formularP3', {
	    url: "/formularP3",
	    templateUrl: 'app/formulars/formularP3.html',
	     controller: 'HomeController'
	  })
	  
	  .state('formularP4', {
	    url: "/formularP4",
	    templateUrl: 'app/formulars/formularP4.html',
	     controller: 'HomeController'
	  });
	 
  
  // Default view to show
  $urlRouterProvider.otherwise('login');
})

 
 .run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
      /*
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    */
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});

