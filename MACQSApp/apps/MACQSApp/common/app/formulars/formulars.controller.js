angular
    .module('starter.controllers')
    .controller('FormularsController', FormularsController);

FormularsController.$inject = ['$scope', '$state', 'HomeService',  '$location']; 
// $scope, $state, HomeService,$location

function FormularsController($scope, $state, HomeService , $location) {
	
	$scope.getFormularP2 = function(){
		console.log(" Go to Formular 2");
		  $state.go('formularP2');
		  
	  }
	  
	  $scope.getFormularP3 = function(){
		  console.log(" Go to Formular 3");
		  
		  $state.go('formularP3');
		  
	  }
	  $scope.getFormularP4 = function(){
		  console.log(" Go to Formular 4");
		  $state.go('formularP4');
		  
	  }
	  
	  
	  $scope.getHome = function(){
		  console.log(" Go to home ");
		  $state.go('home');
		  
	  }
	  
	
}