(function () {
    'use strict';   

angular
    .module('starter.controllers')
    .controller('PopoverController', PopoverController);

PopoverController.$inject = ['$scope', '$ionicPopover']; 
// $scope, $state, HomeService,$location

function PopoverController($scope, $ionicPopover) {
	 // .fromTemplate() method
	//  var template = '<ion-popover-view><ion-header-bar> <h1 class="title">My Popover Title</h1> </ion-header-bar> <ion-content> Hello! </ion-content></ion-popover-view>';

//	  $scope.popover = $ionicPopover.fromTemplate(template, {
//	    scope: $scope
//	  });
	console.log(" geht los mit popover laden");
	  var vm = this;
	  // .fromTemplateUrl() method
//	  $ionicPopover.fromTemplateUrl('app/formulars/formular.popover.html', {
//	    scope: $scope
//	  }).then(function(popover) {
//	    $scope.popover = popover;
//	  });
     
//	  $scope.getVarbType = function(){
//		  console.log(" geht los mit popover laden");
//		  $scope.errortype = true;
//	  }

	  $scope.showPopover = function($event) {
		  console.log(" popover show ");
//	    $scope.popover.show($event);
		  $ionicPopover.fromTemplateUrl('app/formulars/formular.popover.html', {
			    scope: $scope
			  }).then(function(popover) {
				  popover.show(".popover");
			  });
	  };
	  
	  $scope.closePopover = function($event) {
		  console.log(" popover hide ");
		  
		  $ionicPopover.fromTemplateUrl('app/formulars/formular.popover.html', {
			    scope: $scope
			  }).then(function(popover) {
				  popover.hide(".popover");
			  });
	  };
//	  //Cleanup the popover when we're done with it!
//	  $scope.$on('$destroy', function() {
//	    $scope.popover.remove();
//	  });
//	  // Execute action on hide popover
//	  $scope.$on('popover.hidden', function() {
//	    // Execute action
//	  });
//	  // Execute action on remove popover
//	  $scope.$on('popover.removed', function() {
//	    // Execute action
//	  });
	
}
})();