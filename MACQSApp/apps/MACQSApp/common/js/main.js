//var ORDER_COLLECTION_NAME = 'orders',
//			KEY_VALUE_COLLECTION_NAME = 'keyvalue';

var USER_COLLECTION_NAME = 'userCredentials';
var ORDER_COLLECTION_NAME = 'orderNumbers';
var FETO_COLLECTION_NAME = 'fetos';
var FORMULAR1_COLLECTION_NAME = 'formular1';
var TESTVARI  ; 
//User credentials.
var username = null, password = null;

// Offline login state.
var offlineLoggedIn = false;
var ONLINE = false ;

//JSONStore.
//var USER_COLLECTION_NAME = 'userCredentials';
var collections = {
	userCredentials : {
		searchFields : {
			collectionNotEmpty: 'string'
		}
	},
	fetos : {
		searchFields: {"name":"string"},
		adapter : {
		      name: 'fetos',
		      add: 'addFeto',
		  		remove: 'deleteFeto',
		  		replace: 'updateFeto',
		  		load: {
		        procedure: 'getFetos',
		    		params: [],
		    		key: 'array'
		  		}
		  	}
	},
	
	orderNumbers : {
		searchFields: {"fanr":"string"},
		adapter : {
		      name: 'fanrs',
		      add: 'addFanr',
		  		remove: 'deleteFanr',
		  		replace: 'updateFanr',
		  		load: {
		        procedure: 'getFanrs',
		    		params: [],
		    		key: 'array'
		  		}
		  	}
	},
	
//	formularHead : {
//		searchFields: {"feto":"string" , "fanr":"string"},
//		adapter : {
//		      name: 'FormularHead',
//		      add: 'addFFormularHead',
//		  		remove: 'deleteFormularHead',
//		  		replace: 'updateFormularHead',
//		  		load: {
//		        procedure: 'getFormularHeads',
//		    		params: [],
//		    		key: 'array'
//		  		}
//		  	}
//	},
	
	formular1 : {
		searchFields: {"feto":"string" , "fanr":"string"},
		adapter : {
		      name: 'formularHead',
		      add: 'addFFormularHead',
		  		remove: 'deleteFormularHead',
		  		replace: 'updateFormularHead',
		  		load: {
		        procedure: 'getFormularHeads',
		    		params: [],
		    		key: 'array'
		  		}
		  	}
	}
	

	
};


function wlCommonInit(){
	/*
	 * Use of WL.Client.connect() API before any connectivity to a MobileFirst Server is required. 
	 * This API should be called only once, before any other WL.Client methods that communicate with the MobileFirst Server.
	 * Don't forget to specify and implement onSuccess and onFailure callback functions for WL.Client.connect(), e.g:
	 *    
	 *    WL.Client.connect({
	 *    		onSuccess: onConnectSuccess,
	 *    		onFailure: onConnectFailure
	 *    });
	 *     
	 */
	
	document.addEventListener(WL.Events.WORKLIGHT_IS_CONNECTED, connectDetected, false); 
	document.addEventListener(WL.Events.WORKLIGHT_IS_DISCONNECTED, disconnectDetected , false);
	
	// Common initialization code goes here
	
	var env = WL.Client.getEnvironment();
    if(env === WL.Environment.IPHONE || env === WL.Environment.IPAD){
        document.body.classList.add('platform-ios'); 
    } else if(env === WL.Environment.ANDROID){
        document.body.classList.add('platform-android'); 
    }

    WL.Client.connect({
    onSuccess:function() {
    	// onlineLo = true ; 
        console.log("Connected to MFP : i want to do synchronizatiion with push and pull  here. when device is not use and net work connectivity is ok.");
        //
        angular.element(document).ready(function() {
            angular.bootstrap(document, ['starter']);
            // for test offline
         //   testCollection ();
  
            
            
            // hier kÃ¶nnte ich die Synchronisation with Backenend machen..... 18.05.2016 mit Stephane reden
        });         
    }, 
    onFailure:function(f) {
        console.log("Failed to connect to MFP, not sure what to do now.", f); 
    },
    timeout: 5000
    });
    
  //  WL.Device.getNetworkInfo(
    		
//    		isAirplaneMode â€“ true/false
//    		carrierName â€“ string (for example, AT&T or VERIZON)
//    		telephonyNetworkType â€“ string (for example, UMTS or GPRS)
//    		isRoaming â€“ true/false
//    		networkConnectionType â€“ mobile/WiFi
//    		ipAddress â€“ string
//    		isNetworkConnected â€“ true/false
    	//	);



    
    $("#goToSecuredArea").on("click", goToSecuredArea);
	$("#backToUnsecuredArea").on("click", displayUnsecuredArea);
	$("#logout").bind("click", logout);
	$("#cancelLoginButton").bind("click", logout);
	    
}


function testCollection(){
	console.log(" Collections load ");

	//Clear all data inside the collection
//WL.JSONStore.get(USER_COLLECTION_NAME).clear();
// WL.JSONStore.get(ORDER_COLLECTION_NAME).clear();
// WL.JSONStore.get(FETO_COLLECTION_NAME).clear();
			
	
//	WL.JSONStore.get(USER_COLLECTION_NAME).count()
//	.then(function (countResult) {
//	  if(countResult === 0) {
//		  console.log(" also laden and add data :");
//	    //collection is empty, add data
//	   // WL.JSONStore.get(USER_COLLECTION_NAME).add([{name: 'carlos'}, {name: 'mike'}])
////	    .then(function () {
////	      //data stored succesfully
////	    });
//	  }
//	}); 
				
	
	
//	WL.JSONStore.init(USER_COLLECTION_NAME)	
//	.then(function() {
//	// we can make a procedur call or load ...
//WL.JSONStore.get(USER_COLLECTION_NAME).load().then(function (loadedDocuments) {
//	 console.log("users are succefully geladen " , loadedDocuments);
//	}).fail(function (error) {
//		console.log("users kÃ¶nnte nicht laden  ");
//	}) ; 
//	});
 
	
	WL.JSONStore.init(ORDER_COLLECTION_NAME)	
	.then(function() {
WL.JSONStore.get(ORDER_COLLECTION_NAME).load().then(function (loadedDocuments) {
	 console.log("orders are succefully geladen " , loadedDocuments);
	}).fail(function (error) {
		console.log("ORDERS kÃ¶nnte nicht laden  ");
	}) ; 
	});
	
	WL.JSONStore.init(FETO_COLLECTION_NAME)	
	.then(function() {
WL.JSONStore.get(FETO_COLLECTION_NAME).load().then(function (loadedDocuments) {
	 console.log("fetos  are succefully geladen " , loadedDocuments);
	}).fail(function (error) {
		console.log("FETOS kÃ¶nnte nicht laden  ");
	}) ; 
	  
	});	
}



function goToSecuredArea() {
	console.log("Test goToSecuredArea bouton ");
	WL.Device.getNetworkInfo(
		function (networkInfo) {
			console.log("Test WL.Device.getNetworkInfo  " , networkInfo);
			
			console.log("Test Device  networkInfo.isNetworkConnected   " , networkInfo.isNetworkConnected );
			if (networkInfo.isNetworkConnected == "true") {
				//onlineAuthentication();
			} else {
				//offlineAuthentication();
			}
		}
	);
	
	
	// for until device will be defined ... 31.05.2016
	if(onlineLo===true){
		console.log("There is connectionnnnn");
		//offlineAuthentication();
		onlineAuthentication();
	} else {
		offlineAuthentication();
	}
	
}

//Handle the online authentication flow
function onlineAuthentication() {
	console.log(" onlineAuthentication  will be excecuted...");
	/*
	 * Check whether the user is already authenticated.
	 * true : Display the secured area
	 * false: Display the login form; start authentication via the challenge handler
	 */
	console.log("Online auth User :  ", username);
	
	if (WL.Client.isUserAuthenticated("LDAPRealm")) {
		console.log("Online auth User :  ");
		/*
		 * If connected to the Internet and the user is authenticated in the backend, but the username variable is empty, assume a logout was performed while offline
		 * Logout the user from the realm before trying to login again while online.
		 */
		if (username == null) {
			WL.Client.logout("LDAPRealm", {
				onSuccess: function() {
				    onlineAuthentication();
				},
				onFailure:function() {
					WL.SimpleDialog.show("Logout", "Unable to logout at this time. Try again later.", [{text: "OK", handler: function() { }}]); 
				}
			});
		// If connected to the Internet and the user is authenticated in the backend, and the username variable is not empty, display the secured area.
		} else {
			// wird login formular angezeigt
			displaySecuredArea();	
		}
	// If logging-in while offline and Internet connectivity is then available, login to the realm without showing the login form.
	} else if (username) {
		
		
//		var invocationData = {
//			adapter : "authenticationAdapter",
//			procedure : "submitAuthentication",
//			parameters : [$("#username").val(), $("#password").val()]
//		};
//		
//		myCustomRealmChallengeHandler.submitAdapterAuthentication(invocationData, {onSuccess:onlineAuthenticationSuccess, onFailure:onlineAuthenticationFailure });
		
		var reqURL = '/j_security_check';
	    var options = {};
	    options.parameters = {
	    		j_username : $('#username').val(),
	    		j_password : $('#password').val()
	    };
	    options.headers = {};
	    LDAPRealmChallengeHandler.submitLoginForm(reqURL, options, LDAPRealmChallengeHandler.submitLoginFormCallback);
		
		// hier brauche ich myLDAPRealmChalenge...
	} else {
		// The user did not previously log-in in the current session, begin the challenge handler (in common\js\authenticationChallenge.js).
		WL.Client.login("LDAPRealm", {
		onSuccess: onlineAuthenticationSuccess,
		onFailure: onlineAuthenticationFailure
		});	
	}
	
	LDAPRealmChallengeHandler.submitLoginFormCallback = function(response) {
	    var isLoginFormResponse = LDAPRealmChallengeHandler.isCustomResponse(response);
	    if (isLoginFormResponse){
	    	//LDAPRealmChallengeHandler.handleChallenge(response);
	    	console.log(" Online Authentification failed: ");
	    	//$("#authInfo").html("Invalid credentials. Please try again."); 

	    	// for test sehr wichtig bis authentification mit adapter lÃ¤uft
	    	onlineAuthenticationSuccess();
	    	
	    } else {
	    	
	    	
//	    	$('#AppDiv').show();
//	    	$('#AuthDiv').hide();
//	    	LDAPRealmChallengeHandler.submitSuccess();
	    	
	    	onlineAuthenticationSuccess();
	    }
	};
}

function onlineAuthenticationFailure() {
	// If there is Internet connection, but MobileFirst Server is not reachable...
	console.log(" onlineAuthenticationFailure ()");
	offlineAuthentication();
}

// Handle a successful online authentication.
function onlineAuthenticationSuccess() {
	username = $("#username").val();
	password = $("#password").val();
	// sttingof LTPA token as HttP
	console.log("onlineAuthenticationSuccess collection will be created  for username :" , username);
	
	WL.JSONStore.init(collections, {password:password, username:username, localKeyGen:true})
	.then(function() {
		
		console.log("collection is sucessful  created  for username :" , username );
		return WL.JSONStore.get(USER_COLLECTION_NAME).findAll();
	})	
	.then(function(findAllResult) {
		if (findAllResult.length == 0) {
			// The 	JSONStore collection is empty, populate it.
			var data = [{collectionNotEmpty:"true"}];
			return WL.JSONStore.get(USER_COLLECTION_NAME).add(data);
		}
		/* if findAllResult.length > 0 ...
		 * The user store is already populated, don't add again.
		 * This is required in order to verify the user when logging-in offline.
		 * 
		 * Also helps in keeping the JSONStore collection at just 1 record in case of repeated online authentications 
		 * so to not unnecessarily populate it with additional records.
		 */ 
	})
	.then(function() {
		// from me at 03.05.2016
		loadDataForUser(username,password);
		displaySecuredArea();
		
	})	
}

/*
 * Handle offline authentication.
 * If already logged in, display the secure area, otherwise:
 * Try to open the JSONStore with the used password 
 * If successful, display the secured area
 */
function offlineAuthentication() {
	console.log("then offlineAuthentication wird aufgerufen..." , username );
	$("#username").val('');
	$("#password").val('');
	$("#authInfo").empty();
	
	// Don't show the login form  if already offline-logged-in via JSONStore, or previously logged-in online (but have yet logged-out).
	if (offlineLoggedIn === true || username) {
		offlineLoggedIn = true;
		displaySecuredAreaOffline();
	} else {
		// Prepare the login form.
	    $("#unsecuredDiv").hide();
	    $("#authDiv").show();
		$("#offlineLoginButton").show();
		$("#onlineLoginButton").hide();
		$("#offlineLoginButton").unbind("click");
	    $("#offlineLoginButton").bind("click", function() {
	    	
	  				// Don't allow empty username/password field.
	    	if (($("#username").val() == '') || ($("#password").val() == '')) {
				$("#authInfo").html("Invalid credentials. Please try again from empty????.");
			} else {
				console.log("before init collection  pwd: " , $("#password").val());
				
				
		    	WL.JSONStore.init(collections, {password:$("#password").val(), username:$("#username").val(), localKeyGen:true})	
		    	.then(function() {
		    		
		    		/*
					 * Need to handle the case where logging in when offline for the first time, in which case logging in cannot be done as there wouldn't be a
					 * JSONStore available yet.
					 */
		    		WL.JSONStore.get(USER_COLLECTION_NAME).count()
		    		.then(function(countResult) {
		    			if (countResult == 0) {
		    				WL.JSONStore.destroy($("#username").val());
			                $("#authInfo").html("First time login must be done when Internet connection is available.");
                    
			             //   displaySecuredAreaOffline();   //			Simone Test displaySecuredAreaOffline();
						    $("#username").val('');
						    $("#password").val('');
					    } else {
					    	// Preserve the username and password, set the offline-logged-in flag and dislay the secured area.
		                    offlineLoggedIn = true;
		                    username = $("#username").val();
		        	    	password = $("#password").val();
		                    displaySecuredAreaOffline();
					    }
		    		})
		    	})
		    	.fail(function() {
		    		$("#authInfo").html("Invalid credentials. Please try again. kommt from JSONStore log ");
		    		$("#username").val('');
		    		$("#password").val('');
		    	})
			}
		});
	}
}

function logout() {
	password = null;
	username = null;
	WL.JSONStore.closeAll();
//	offlineLoggedIn = false;
//	WL.Device.getNetworkInfo(
//		function (networkInfo) {
//			if (networkInfo.isNetworkConnected == "true") {
//				WL.Client.logout("LDAPRealm", {onSuccess:displayUnsecuredArea, onFailure:displayUnsecuredArea});
//			}
//		}
//	);
	
	displayUnsecuredArea();
}
//init data from jsonstore for user 03.05.2016
function loadDataForUser(username, password){
// muss noch besser Ã¼berlegen
WL.JSONStore.get(FETO_COLLECTION_NAME).load().then(function (loadedDocuments) {
	 console.log(  loadedDocuments ,"fetos  are succefully geladen  for " , username);
	}).fail(function (error) {
		console.log("FETOS kÃ¶nnte nicht laden  ");
	}) ; 
	  
	//});	
	

WL.JSONStore.get(ORDER_COLLECTION_NAME).load().then(function (loadedDocuments) {
	 console.log("orders are succefully geladen " , loadedDocuments);
	}).fail(function (error) {
		console.log("ORDERS kÃ¶nnte nicht laden  ");
	}) ; 
}


// Display the secured and unsecured areas.
function displaySecuredArea() {
	console.log(" displaySecuredArea will be excecuted...");
	$("#authDiv").hide();
	$("#unsecuredDiv").hide();
	$("#securedDiv").show();  // $state.go('home');   $location.path('formularP1');
	$("#offlineAccess").hide();
//	$state.go('home'); 
}

function displaySecuredAreaOffline() {
	$("#offlineAccess").show();
	$("#securedDiv").addClass("displaySecuredAreaOffline");
	$("#authDiv").hide();
	$("#unsecuredDiv").hide();
	$("#securedDiv").show();
}

function displayUnsecuredArea() {
	$("#offlineAccess").hide();
	$("#securedDiv").removeClass("displaySecuredAreaOffline");
    $("#securedDiv").hide();
    $("#unsecuredDiv").show();
}



// 02.06.2016

// 18.05.2016 offline modus

function connectionFailure(){
	alert("Could not connect to the MobileFirst Server.");
	var output = new Date() + "<hr />Working offline";
	$('#info').html(output);
}

function disconnectDetected(){
	var output = new Date() + "<hr />disconnectDetected";
	$('#info').html(output);
}

function connectDetected(){
	var output = new Date() + "<hr />connectDetected";
	$('#info').html(output);
}

function setHeartBeatInterval(interval){
	var output = new Date() + "<hr />heartbeat interval is set to: " + interval;
	$('#info').html(output);
	WL.Client.setHeartBeatInterval(interval);
}


